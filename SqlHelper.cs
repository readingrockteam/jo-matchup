﻿using System.Data;
using System.Data.SqlClient;

namespace jomatchup
{
    public class SqlHelper
    {
        private SqlConnection _cn;

        public SqlHelper(string connectionString)
        {
            _cn = new SqlConnection(connectionString);
        }

        public bool IsConnected
        {
            get
            {
                if (_cn.State == ConnectionState.Closed)
                    _cn.Open();
                return true;
            }
        }
    }
}
