﻿using System;
using System.Configuration;

namespace jomatchup
{
    public class AppSetting
    {
        private readonly Configuration _config;

        public AppSetting()
        {
            _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        public string GetConnectionString(string key)
        {
            return _config.ConnectionStrings.ConnectionStrings[key].ConnectionString;
        }

        public void SaveConnectionString(string key, string value)
        {
            _config.ConnectionStrings.ConnectionStrings[key].ConnectionString = value + "MultipleActiveResultSets=true;";
            _config.ConnectionStrings.ConnectionStrings[key].ProviderName = "System.Data.SqlClient";
            try
            {
                _config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
