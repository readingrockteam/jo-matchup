﻿

using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace jomatchup.Models
{
    public class shitem
    {
        [Key]
        public string fsokey { get; set; }
        public string fac { get; set; }
        public string sfac { get; set; }
        public decimal fshipqty { get; set; }
    }
}
