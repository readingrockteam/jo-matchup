using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using jomatchup.Models.Mapping;

namespace jomatchup.Models
{
    public interface IContext
    {
        IDbSet<AppError> AppErrors { get; set; }
            //DbSet<TEntity> SqlQuery<TEntity>() where TEntity : class;
        Database db { get; set; }
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        //DbSqlQuery<TEntity> SqlQuery<TEntity>() where TEntity : class;


        IDbSet<soitem> soitems { get; set; }
        IDbSet<shitem> shitems { get; set; }
        IDbSet<jomast> jomasts { get; set; }


        int SaveChanges();

    }
    public class PackingSolutionContext : DbContext, IContext
    {
        static PackingSolutionContext()
        {
            Database.SetInitializer<PackingSolutionContext>(null);
        }

        public PackingSolutionContext()
            : base("Name=PackingSolutionContext")
        {
        }

        public IDbSet<AppError> AppErrors { get; set; }

        public Database db
        {
            get { return Database; }
            set { throw new NotImplementedException(); }
        }

        public IDbSet<soitem> soitems { get; set; }
        public IDbSet<shitem> shitems { get; set; }
        public IDbSet<jomast> jomasts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new app_errorsMap());
        }




        public override int SaveChanges()
        {
         //   var modifiedEntries = ChangeTracker.Entries()
         //.Where(x => x.Entity is IAuditableEntity
         //    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

         //   foreach (var entry in modifiedEntries)
         //   {
         //       IAuditableEntity entity = entry.Entity as IAuditableEntity;
         //       if (entity != null)
         //       {
         //           string identityName = Thread.CurrentPrincipal.Identity.Name;
         //           DateTime now = DateTime.UtcNow;

         //           if (entry.State == System.Data.Entity.EntityState.Added)
         //           {
         //               entity.CreatedBy = identityName;
         //               entity.CreatedDate = now;
         //           }
         //           else
         //           {
         //               base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
         //               base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
         //           }

         //           entity.UpdatedBy = identityName;
         //           entity.UpdatedDate = now;
         //       }
         //   }

            return base.SaveChanges();
        }

    }
}
