﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace jomatchup.Models
{
    public class soitem
    {
        [Key, Column(Order = 1)]
        public string fsono { get; set; }
        [Key, Column(Order = 2)]
        public string finumber { get; set; }
        public string fpartno { get; set; }
        public decimal fquantity { get; set; }
        public string fdelivery { get; set; }
        public string fordername { get; set; }
        public string fstatus { get; set; }
        public DateTime fduedate { get; set; }
        public decimal fbook { get; set; }
        public decimal fproqty { get; set; }
        public decimal fonhand { get; set; }
        public DateTime forderdate { get; set; }
        public string fsocoord { get; set; }
        public string frelease { get; set; }
        public bool fmultiple { get; set; }
        public string fac { get; set; }
        public string fsokey { get; set; }
        public string sfac { get; set; }
    }
}
