﻿using System.ComponentModel.DataAnnotations;


namespace jomatchup.Models
{
    public class jomast
    {
        [Key]
        public string fjobno { get; set; }
        public string fpartno { get; set; }
        public decimal fmqty { get; set; }
        public string fstatus { get; set; }
        public string fsono { get; set; }
        public string finumber { get; set; }
        public string frelease { get; set; }
        public string fac { get; set; }
    }
}
