﻿using System.Data.Entity.ModelConfiguration;

namespace jomatchup.Models.Mapping
{
    public class app_errorsMap : EntityTypeConfiguration<AppError>
    {
        public app_errorsMap()
        {
            HasKey(t => new {t.ID});

            // properties
            Property(t => t.Facility).HasMaxLength(10);
            Property(t => t.Application).HasMaxLength(50);
            Property(t => t.User).HasMaxLength(50);

            // table and column mappings
            ToTable("app_errors");
            Property(t => t.ID).HasColumnName("identity_column");
            Property(t => t.Facility).HasColumnName("fac");
            Property(t => t.Application).HasColumnName("application");
            Property(t => t.Error).HasColumnName("catch_error");
            Property(t => t.User).HasColumnName("user_id");
            Property(t => t.SysDate).HasColumnName("sys_date");

        }
    }
}
