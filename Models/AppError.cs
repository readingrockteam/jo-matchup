﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace jomatchup.Models
{
    [Table("app_errors")]
    public class AppError
    {
        public int ID { get; set; }
        public string Facility { get; set; }
        public string Application { get; set; }
        public string Error { get; set; }
        public string User { get; set; }
        [Display(Name="Date")]
        public DateTime SysDate { get; set; }
    }
}
