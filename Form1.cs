﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using jomatchup.Forms;
using jomatchup.Models;
using jomatchup.Properties;
using PackingSolutionContext = jomatchup.Models.PackingSolutionContext;

namespace jomatchup
{
    public partial class Form1 : Form
    {

        private string _catalog = string.Empty;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            using (var context = new PackingSolutionContext())
            {
                var shitems = context.db.SqlQuery<shitem>("exec spGetJomatchupShitem").ToList();
                var soitems = context.db.SqlQuery<soitem>("exec spGetJomatchupSoitem").OrderBy(x => x.forderdate).ToList();
                if (soitems.Count == 0)
                {
                    MessageBox.Show(
                        "There are no open Sales Orders to create jobs for. Check with your PM to see if they opened the SO.");
                    return;
                }
                var d = soitems[0].forderdate;
                var p = new SqlParameter("@fopen_dt", d);
                var jomasts = context.db.SqlQuery<jomast>("exec spGetJomatchupJomast @fopen_dt", p).ToList();
                var ds = new DataSet {DataSetName = "M2mdata01Dataset"};
               
                var dt = tools.EmailTools.ConvertToDataTable(shitems);
                dt.TableName = "shitem";
                ds.Tables.Add(dt);
                dt = tools.EmailTools.ConvertToDataTable(soitems);
                dt.TableName = "soitem";
                ds.Tables.Add(dt);
                dt = tools.EmailTools.ConvertToDataTable(jomasts);
                dt.TableName = "jomast";
                ds.Tables.Add(dt);

                var rpt = new rptNeededMulti {FileName = Settings.Default.rptDir + @"\rptNeededMulti.rpt"};
                var outputfile = Settings.Default.uploadDir + @"\jo needed " + _catalog.Replace("'", "") + ".xls";
                rpt.SetDataSource(ds);

                rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, outputfile);

                MessageBox.Show("Done!!", "finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void packingSolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var obj = new frmSqlConfig { Title = "PackingSolutionContext" };
            obj.ShowDialog(this);
        }

        private void m2MUploadDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var obj = new FolderBrowserDialog {Description = "M2M Upload Folder (excel file)"};
            if (obj.ShowDialog(this) == DialogResult.OK)
            {
                Settings.Default.uploadDir = obj.SelectedPath;
                Settings.Default.Save();
            }
        }

        private void crystalReportsDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var obj = new FolderBrowserDialog {Description = "Crystal Reports Folder"};
            if (obj.ShowDialog(this) == DialogResult.OK)
            {
                Settings.Default.rptDir = obj.SelectedPath;
                Settings.Default.Save();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var obj = new AboutBox1();
            obj.ShowDialog(this);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var appSetting = new AppSetting();
            var connStr = appSetting.GetConnectionString("PackingSolutionContext");

            var connArr = connStr.Split(';');
            GetSource(connArr, out _catalog);
            Text = "08 Jo MatchUp v. " + Assembly.GetExecutingAssembly().GetName().Version + "   PS: " + _catalog.Replace("'", "");
            //MessageBox.Show("you are connected to: " + connStr);
        }

        private void GetSource(string[] ConnArr, out string Source)
        {
            Source = string.Empty;
            foreach (var el in ConnArr)
            {
                if (el.Contains("Catalog"))
                    Source = el.Split('=')[1];
            }
        }

        private void showConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var appSetting = new AppSetting();
            var connStr = appSetting.GetConnectionString("PackingSolutionContext");
            MessageBox.Show(connStr);
        }
        private void showUploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Settings.Default.uploadDir);
        }

        private void showRptsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Settings.Default.rptDir);
        }
    }
}
