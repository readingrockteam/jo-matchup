﻿namespace jomatchup
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.packingSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m2MUploadDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crystalReportsDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnProcess = new System.Windows.Forms.Button();
            this.showConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showUploadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showRptsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.menuStrip1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnProcess);
            this.splitContainer1.Size = new System.Drawing.Size(466, 124);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.adminToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(466, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.packingSolutionToolStripMenuItem,
            this.m2MUploadDirectoryToolStripMenuItem,
            this.crystalReportsDirectoryToolStripMenuItem});
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.adminToolStripMenuItem.Text = "Admin";
            // 
            // packingSolutionToolStripMenuItem
            // 
            this.packingSolutionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showConnectionToolStripMenuItem});
            this.packingSolutionToolStripMenuItem.Name = "packingSolutionToolStripMenuItem";
            this.packingSolutionToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.packingSolutionToolStripMenuItem.Text = "Packing Solution";
            this.packingSolutionToolStripMenuItem.Click += new System.EventHandler(this.packingSolutionToolStripMenuItem_Click);
            // 
            // m2MUploadDirectoryToolStripMenuItem
            // 
            this.m2MUploadDirectoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showUploadToolStripMenuItem});
            this.m2MUploadDirectoryToolStripMenuItem.Name = "m2MUploadDirectoryToolStripMenuItem";
            this.m2MUploadDirectoryToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.m2MUploadDirectoryToolStripMenuItem.Text = "M2M Upload Directory";
            this.m2MUploadDirectoryToolStripMenuItem.Click += new System.EventHandler(this.m2MUploadDirectoryToolStripMenuItem_Click);
            // 
            // crystalReportsDirectoryToolStripMenuItem
            // 
            this.crystalReportsDirectoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showRptsToolStripMenuItem1});
            this.crystalReportsDirectoryToolStripMenuItem.Name = "crystalReportsDirectoryToolStripMenuItem";
            this.crystalReportsDirectoryToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.crystalReportsDirectoryToolStripMenuItem.Text = "Crystal Reports Directory";
            this.crystalReportsDirectoryToolStripMenuItem.Click += new System.EventHandler(this.crystalReportsDirectoryToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Location = new System.Drawing.Point(12, 3);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(442, 85);
            this.btnProcess.TabIndex = 0;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // showConnectionToolStripMenuItem
            // 
            this.showConnectionToolStripMenuItem.Name = "showConnectionToolStripMenuItem";
            this.showConnectionToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.showConnectionToolStripMenuItem.Text = "Show Connection";
            this.showConnectionToolStripMenuItem.Click += new System.EventHandler(this.showConnectionToolStripMenuItem_Click);
            // 
            // showUploadToolStripMenuItem
            // 
            this.showUploadToolStripMenuItem.Name = "showUploadToolStripMenuItem";
            this.showUploadToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.showUploadToolStripMenuItem.Text = "show upload dir";
            this.showUploadToolStripMenuItem.Click += new System.EventHandler(this.showUploadToolStripMenuItem_Click);
            // 
            // showRptsToolStripMenuItem1
            // 
            this.showRptsToolStripMenuItem1.Name = "showRptsToolStripMenuItem1";
            this.showRptsToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.showRptsToolStripMenuItem1.Text = "show rpts dir";
            this.showRptsToolStripMenuItem1.Click += new System.EventHandler(this.showRptsToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 124);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem packingSolutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m2MUploadDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crystalReportsDirectoryToolStripMenuItem;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showUploadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showRptsToolStripMenuItem1;
    }
}

